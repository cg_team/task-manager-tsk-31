package ru.inshakov.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
