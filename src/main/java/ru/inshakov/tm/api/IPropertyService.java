package ru.inshakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthor();

    @NotNull
    String getAuthorEmail();

    @Nullable
    String getFileBase64Path();

    @Nullable
    String getFileBinaryPath();

    @Nullable
    String getFileJsonPath(@Nullable String jsonType);

    @Nullable
    String getFileXmlPath(@Nullable String xmlType);

    @Nullable
    String getFileYamlPath();

}
