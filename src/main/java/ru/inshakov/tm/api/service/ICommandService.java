package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @Nullable
    AbstractCommand getCommandByArg(@NotNull String arg);

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<AbstractCommand> getCommands();

    void add(@Nullable AbstractCommand command);

    @NotNull Collection<String> getCommandArgs();

    @NotNull Collection<AbstractCommand> getArgsCommands();
}
