package ru.inshakov.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public final class UnknownArgumentException extends RuntimeException {

    public UnknownArgumentException(@Nullable final String arg) {
        super("Error! Argument ``" + arg + "`` not found...");
    }

}
