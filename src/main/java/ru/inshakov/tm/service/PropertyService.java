package ru.inshakov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String VERSION_KEY = "application.version";

    @NotNull
    private static final String VERSION_DEFAULT = "";

    @NotNull
    private static final String AUTHOR_KEY = "author";

    @NotNull
    private static final String AUTHOR_DEFAULT = "";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_EMAIL_DEFAULT = "";

    @NotNull
    private static final String FILE_BASE64 = "filePath.base64";

    @NotNull
    private static final String FILE_BINARY = "filePath.binary";

    @NotNull
    private static final String FILE_FASTERXML_JSON = "filePath.json";

    @NotNull
    private static final String FILE_FASTERXML_XML = "filePath.xml";

    @NotNull
    private static final String FILE_FASTERXML_YAML = "filePath.yaml";

    @NotNull
    private static final String FILE_JAXB_JSON = "filePath.jaxb-json";

    @NotNull
    private static final String FILE_JAXB_XML = "filePath.jaxb-xml";

    @NotNull
    private static final String FILE_BACKUP_XML = "filePath.backup-xml";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (System.getenv().containsKey(VERSION_KEY)) {
            return System.getenv(VERSION_KEY);
        }
        if (System.getProperties().contains(VERSION_KEY)) {
            return System.getProperty(VERSION_KEY);
        }
        return properties.getProperty(VERSION_KEY, VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getAuthor() {
        if (System.getenv().containsKey(AUTHOR_KEY)) {
            return System.getenv(AUTHOR_KEY);
        }
        if (System.getProperties().contains(AUTHOR_KEY)) {
            return System.getProperty(AUTHOR_KEY);
        }
        return properties.getProperty(AUTHOR_KEY, AUTHOR_DEFAULT);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        if (System.getenv().containsKey(AUTHOR_EMAIL_KEY)) {
            return System.getenv(AUTHOR_EMAIL_KEY);
        }
        if (System.getProperties().contains(AUTHOR_EMAIL_KEY)) {
            return System.getProperty(AUTHOR_EMAIL_KEY);
        }
        return properties.getProperty(AUTHOR_EMAIL_KEY, AUTHOR_EMAIL_DEFAULT);
    }

    @Nullable
    @Override
    public String getFileBase64Path() {
        return properties.getProperty(FILE_BASE64);
    }


    @Nullable
    @Override
    public String getFileBinaryPath() {
        return properties.getProperty(FILE_BINARY);
    }

    @Override
    public @Nullable String getFileJsonPath(@Nullable final String jsonType) {
        if (jsonType == "jaxb") return properties.getProperty(FILE_JAXB_JSON);
        else return properties.getProperty(FILE_FASTERXML_JSON);
    }

    @Override
    public @Nullable String getFileXmlPath(@Nullable String xmlType) {
        if (xmlType == "jaxb") return properties.getProperty(FILE_JAXB_XML);
        else if (xmlType == "backup") return properties.getProperty(FILE_BACKUP_XML);
        else return properties.getProperty(FILE_FASTERXML_XML);
    }

    @Override
    public @Nullable String getFileYamlPath() {
        return properties.getProperty(FILE_FASTERXML_YAML);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        if (System.getenv().containsKey(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value = System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getProperties().contains(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value = System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        @NotNull final String value = properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY)) return System.getenv(PASSWORD_SECRET_KEY);
        if (System.getProperties().contains(PASSWORD_SECRET_KEY)) return System.getProperty(PASSWORD_SECRET_KEY);
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

}
