package ru.inshakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.api.repository.ICommandRepository;

import java.util.*;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArgsCommands() {
        final List<AbstractCommand> result = new ArrayList<>();
        for (final AbstractCommand command: commands.values()) {
            @Nullable final String arg = command.arg();
            if(arg == null  || arg.isEmpty()) continue;
            result.add(command);
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<String> getCommandArgs() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command: commands.values()) {
            @Nullable final String arg = command.arg();
            if(arg == null  || arg.isEmpty()) continue;
            result.add(arg);
        }
        return result;
    }

    public void add(@NotNull final AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (Optional.ofNullable(arg).isPresent()) arguments.put(arg, command);
        commands.put(name, command);
    }

    public AbstractCommand getCommandByArg(@NotNull final String name) {
        return arguments.get(name);
    }

}
