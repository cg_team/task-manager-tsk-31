package ru.inshakov.tm.util;

public interface ValidationUtil {

    static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

    static boolean checkIndex(int index, int size) {
        if (index < 0) return false;
        return index <= size - 1;
    }

}
