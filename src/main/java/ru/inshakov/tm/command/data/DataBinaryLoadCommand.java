package ru.inshakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.dto.Domain;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.EmptyFilePathException;
import ru.inshakov.tm.service.PropertyService;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Optional;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-load-bin";
    }

    @NotNull
    @Override
    public String description() {
        return "load data from bin file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @Nullable final String filePath = propertyService.getFileBinaryPath();
        Optional.ofNullable(filePath).orElseThrow(EmptyFilePathException::new);
        @NotNull final FileInputStream fileInputStream = new FileInputStream(filePath);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
