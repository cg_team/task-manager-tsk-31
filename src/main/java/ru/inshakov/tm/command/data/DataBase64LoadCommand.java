package ru.inshakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.dto.Domain;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.EmptyFilePathException;
import ru.inshakov.tm.service.PropertyService;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-load-base64";
    }

    @NotNull
    @Override
    public String description() {
        return "load data from base64 file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @Nullable final String filePath = propertyService.getFileBase64Path();
        Optional.ofNullable(filePath).orElseThrow(EmptyFilePathException::new);
        @NotNull final String base64data = new String(Files.readAllBytes(Paths.get(filePath)));
        @NotNull final byte[] decodeData = new BASE64Decoder().decodeBuffer(base64data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodeData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
