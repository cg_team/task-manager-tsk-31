package ru.inshakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractProjectCommand;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-change-status-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "change project status by index";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER ID");
        @NotNull final Integer index = TerminalUtil.nextNumber()-1;
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        @NotNull final Project project = serviceLocator.getProjectService().changeStatusByIndex(userId, index, status);
    }

}
