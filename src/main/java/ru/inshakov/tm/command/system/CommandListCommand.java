package ru.inshakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "command-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @Nullable final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        commands.forEach((command -> System.out.println(command.name())));
    }

}
